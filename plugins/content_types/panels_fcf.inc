<?php

/**
 * @file
 * Panels integration for Field Collection interface.
 */

$plugin = array(
  'single' => TRUE,
  'title' => t('Configurable Field Collection Form'),
  'description' => t('TODO'),
  'category' => t('Form'),
  'edit form' => 'panels_fcf_edit_form',
  'render callback' => 'panels_fcf_render',
  'required context' => new ctools_context_required(t('Entity'), 'entity'),
  'admin info' => 'panels_fcf_admin_info',
  'defaults' => array(
    'context' => '',
    'field_name' => '',
    'destination' => '',
    'substitute' => 0,
  ),
);

/**
 * Admin info callback for panel pane.
 */
function panels_fcf_admin_info($subtype, $conf, $contexts) {

  $block = new stdClass();
  $block->title = $conf['override_title'] ? $conf['override_title_text'] : '';
  $params = array(
    '%entity' => $conf['context'],
    '%fc' => $conf['field_name'],
    '%destination' => $conf['destination'],
  );
  $block->content = t('Field Collection "%fc" form for entity "%entity" with a destination of "%destination".', $params);

  return $block;
}

/**
 * Edit form callback for the content type.
 */
function panels_fcf_edit_form($form, &$form_state) {

  $conf = $form_state['conf'];

  $options = _panels_fcf_load_all_field_collections();
  $form['field_name'] = array(
    '#type' => 'select',
    '#title' => t('Field Collection'),
    '#description' => t('The name of the Field Collection to use.'),
    '#default_value' => $conf['field_name'],
    '#options' => $options,
  );

  $form['destination'] = array(
    '#type' => 'textfield',
    '#title' => t('Destination'),
    '#description' => t('By default, the view page of the field collection item.'),
    '#default_value' => $conf['destination'],
  );

  if (!empty($form_state['contexts'])) {
    // Set extended description if both CCK and Token modules are enabled,
    // notifying of unlisted keywords.
    if (module_exists('content') && module_exists('token')) {
      $description = t('If checked, context keywords will be substituted in this content. Note that CCK fields may be used as keywords using patterns like <em>%node:field_name-formatted</em>.');
    }
    elseif (!module_exists('token')) {
      $description = t('If checked, context keywords will be substituted in this content. More keywords will be available if you install the Token module, see http://drupal.org/project/token.');
    }
    else {
      $description = t('If checked, context keywords will be substituted in this content.');
    }

    $form['substitute'] = array(
      '#type' => 'checkbox',
      '#title' => t('Use context keywords'),
      '#description' => $description,
      '#default_value' => !empty($conf['substitute']),
    );

    $form['contexts'] = array(
      '#title' => t('Substitutions'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );

    $rows = array();
    foreach ($form_state['contexts'] as $context) {
      foreach (ctools_context_get_converters('%' . check_plain($context->keyword) . ':', $context) as $keyword => $title) {
        $rows[] = array(
          check_plain($keyword),
          t('@identifier: @title', array('@title' => $title, '@identifier' => $context->identifier)),
        );
      }
    }
    $header = array(t('Keyword'), t('Value'));
    $form['contexts']['context'] = array(
      '#markup' => theme('table', array(
          'header' => $header,
          'rows' => $rows,
        )
      ),
    );
  }

  return $form;
}

/**
 * The submit form stores the data in $conf.
 */
function panels_fcf_edit_form_submit($form, &$form_state) {

  foreach (array_keys($form_state['plugin']['defaults']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * Run-time rendering of the body of the block (content type).
 */
function panels_fcf_render($subtype, $conf, $panel_args, $context = NULL) {

  $block = new stdClass();

  $block->title = '';
  $block->content = '';

  if (!$context->empty) {
    $entity_type = end($context->type);
    list($entity_id) = entity_extract_ids($entity_type, $context->data);

    // @TODO: someting goes wrong if there are two such panels on one page,
    // and when you submit one of them, both will display the submitted form.
    // The problem must be somewhere in field_collection_item_form().
    $form = field_collection_item_add($conf['field_name'], $entity_type, $entity_id);
    if (!is_numeric($form)) {
      $block->content = drupal_render($form);
    }
    else {
      if (user_access('use panels dashboard')) {
        drupal_set_message(t("Can't display the field collection form."), 'error');
      }
    }
  }

  return $block;
}

/**
 * Helper function to return all the field collections.
 *
 * @return array
 *   Ready to use as #options in a dropdown.
 */
function _panels_fcf_load_all_field_collections() {

  $instances = field_info_instances();
  $field_collections = array();
  foreach ($instances as $type_bundles) {
    foreach ($type_bundles as $bundle_instances) {
      foreach ($bundle_instances as $field_name => $instance) {
        $field = field_info_field($field_name);
        if ($field['type'] == 'field_collection') {
          $field_collections[] = $field_name;
        }
      }
    }
  }

  return drupal_map_assoc($field_collections);
}
